"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const request = require("supertest");
const baseURL = "https://jsonplaceholder.typicode.com/";
describe("GET /user/1", () => {
    // const newTodo = {
    //   id: crypto.randomUUID(),
    //   item: "Drink water",
    //   completed: false,
    // }
    beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
        // set up the todo
        //await request(baseURL).post("/todo").send(newTodo);
    }));
    afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
        //await request(baseURL).delete(`/todo/${newTodo.id}`)
    }));
    it("should return 200", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield request(baseURL).get("users/1");
        //console.log(response.body);
        expect(response.statusCode).toBe(200);
    }));
});
//# sourceMappingURL=api.test.js.map