# TypeScript Jest
Unit testing and integration testing with Jest, Supertest and TypeScript.

[Jest Docs](https://jestjs.io/docs/getting-started)

# Path to Awesomeness
- Clone this repo
- Install dependencies: `npm install`
- Start TypeScript comipiler in watch mode: `tsc --watch`
- Run tests: `npm test`
- Add logic to TypeScript files in `src` directory
- Add tests in `test` directory
- Create code coverage report: `jest --coverage`

# Setting Up Jest
- `npm i -D jest ts-jest @types/jest`
- `npm install -g jest` 
- `npx ts-jest config:init`
- `npm i -D supertest`
- [Reference](https://itnext.io/testing-with-jest-in-typescript-cc1cd0095421)

# Notes
- Jest 29.5
- TypeScript 5.0.4
