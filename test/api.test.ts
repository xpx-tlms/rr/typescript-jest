const request = require("supertest")
const baseURL = "https://jsonplaceholder.typicode.com/"

describe("GET /user/1", () => {
    // const newTodo = {
    //   id: crypto.randomUUID(),
    //   item: "Drink water",
    //   completed: false,
    // }

    beforeAll(async () => {
      // set up the todo
      //await request(baseURL).post("/todo").send(newTodo);
    })
    afterAll(async () => {
      //await request(baseURL).delete(`/todo/${newTodo.id}`)
    })

    it("should return 200", async () => {
      const response = await request(baseURL).get("users/1");
      //console.log(response.body);
      expect(response.statusCode).toBe(200);
    });
});
